![Jekyll Version](https://img.shields.io/gem/v/jekyll.svg)

# Samuel Walker's Personal Website

Hi, welcome to my personal website. It's built using [Jekyll](https://jekyllrb.com/) and hosted on [GitLab Pages](https://about.gitlab.com/features/pages/) using [Firebase](https://firebase.google.com/).

# Theme

This site uses a [Jekyll implementation](https://github.com/volny/creative-theme-jekyll) of the [Creative Theme](http://startbootstrap.com/template-overviews/creative/) template by [Start Bootstrap](http://startbootstrap.com). It was created by @volny.

_Forked from https://gitlab.com/steko/test/_

[`.gitlab-ci.yml`]: https://gitlab.com/jekyll-themes/creative/blob/pages/.gitlab-ci.yml
[`_config.yml`]: https://gitlab.com/jekyll-themes/creative/blob/pages/_config.yml
[`Pages`]: https://gitlab.com/jekyll-themes/creative/tree/pages
